package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Contact;
import com.uantwerp.project_databases.benchmark.db2.entity.Languages;

@Repository("languagesRepository")
public class LanguagesRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final RowMapper<Languages> languageRowMapper = new RowMapper<Languages>() {
		@Override
		public Languages mapRow(ResultSet rs, int rowNum) throws SQLException {
			Languages language = new Languages(rs.getString("CODEID"), rs.getString("LANGUAGE"),
					rs.getString("DESCRIPTION"));
			return language;
		}
	};

	public List<Languages> selectLanguageCodeID(String codeId) {
		if (codeId == null){
			return null;
		}
		return jdbcTemplate.query("SELECT code_id CODEID, LANGUAGE, DESCRIPTION"+
									" FROM schdbenterp.languages"+
									" Where code_id = '"+codeId+"'"
											+ " and language = 'NL'" ,
									languageRowMapper);
	}
	
	public List<Languages> selectLanguageDescription(String description) {
		if (description == null){
			return null;
		}
		return jdbcTemplate.query("SELECT code_id CODEID, LANGUAGE, DESCRIPTION"+
									" FROM schdbenterp.languages"+
									" Where description = '"+description+"'"
											+ " and language = 'NL'" ,
									languageRowMapper);
	}
	

}
