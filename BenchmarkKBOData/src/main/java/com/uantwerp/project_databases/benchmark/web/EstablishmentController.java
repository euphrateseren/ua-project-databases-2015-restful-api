package com.uantwerp.project_databases.benchmark.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.uantwerp.project_databases.benchmark.db2.entity.Adress;
import com.uantwerp.project_databases.benchmark.db2.repo.EstablishmentAddress;
import com.uantwerp.project_databases.benchmark.db2.service.EnterpriseAllService;
import com.uantwerp.project_databases.benchmark.db2.service.EnterpriseService;
import com.uantwerp.project_databases.benchmark.exception.ResponseCodes;
import com.uantwerp.project_databases.benchmark.exception.ServiceResponse;
import com.uantwerp.project_databases.benchmark.utilities.AddressParams;

@RestController
@RequestMapping(value = "kbo_data/api/v1")
/** Class that maps all the web services related to establishments and addresses extends BaseController
*
* @author gerito
* @version 1.0 2015
*/
public class EstablishmentController extends BaseController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(EstablishmentController.class);

	@Autowired
	EnterpriseService enterpriseService;
	@Autowired
	EnterpriseAllService enterpriseAllService;

	/**
	 * Web service which returns the number of establishments of an enterprise and a list of addresses of the enterprise with the enterprise number <br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/establishments/{entityNumber}/establishmentaddresses <br>
	 * @param entityNumber	String with the enterprise number of the enterprise
	 * @return	EstablishmentAddress object: Number of establishments of the enterprise and List of Address object of the establishments
	 */
	@RequestMapping(value = "/establishments/{entityNumber}/establishmentaddresses", method = RequestMethod.GET)
	EstablishmentAddress getEstablishmentCountAndAddress(@PathVariable String entityNumber) {
		return enterpriseService.getEstablishmentCountAndAddress(entityNumber);
	}
	
	/**
	 * Web service which returns the number of establishments of an enterprise and a list of addresses of the enterprise with the enterprise name<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/establishments/{enterpriseName}/establishmentaddresses <br>
	 * @param enterpriseName	String with the enterprise name of the enterprise
	 * @return	EstablishmentAddress object: Number of establishments of the enterprise and List of Address object of the establishments
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/establishmentaddresses", method = RequestMethod.GET)
	EstablishmentAddress getEstablishmentCountAndAddressWithName(@PathVariable String enterpriseName) {
		String entityNumber = enterpriseService.getEntityNumberWithName(enterpriseName);
		return enterpriseService.getEstablishmentCountAndAddress(entityNumber);
	}
	
	/**
	 * Web service which returns the number of establishments of an enterprise with the enterprise number<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/establishments/{entityNumber}/size <br>
	 * @param entityNumber	String with the enterprise name of the enterprise
	 * @return	Number of establishments of the enterprise
	 */
	@RequestMapping(value = "/establishments/{entityNumber}/size", method = RequestMethod.GET)
	int getEstablishmentCount(@PathVariable String entityNumber) {
		return enterpriseService.getEstablishmentCount(entityNumber);
	}
	
	/**
	 * Web service which returns the number of establishments of an enterprise with the enterprise name<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/denominations/{enterpriseName}/establishments/size <br>
	 * @param enterpriseName	String with the enterprise name of the enterprise
	 * @return	Number of establishments of the enterprise
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/establishments/size", method = RequestMethod.GET)
	int getEstablishmentCountWithName(@PathVariable String enterpriseName) {
		String entityNumber = enterpriseService.getEntityNumberWithName(enterpriseName);
		return enterpriseService.getEstablishmentCount(entityNumber);
	}
	
	/**
	 * Web service which returns the address of an enterprise/establishment with the enterprise number<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/establishments/{entityNumber}/addresses <br>
	 * @param entityNumber	String with the enterprise number of the enterprise
	 * @return	Address object
	 */
	@RequestMapping(value = "/establishments/{entityNumber}/addresses", method = RequestMethod.GET)
	List<Adress> getEstablishmentAddresses(@PathVariable String entityNumber) {
		return enterpriseService.getEstablishmentAddresses(entityNumber);
	}
	
	/**
	 * Web service which returns the address of an enterprise/establishment with the enterprise name<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/denominations/{enterpriseName}/establishments/addresses <br>
	 * @param entityNumber	String with the enterprise name of the enterprise
	 * @return	Address object
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/establishments/addresses", method = RequestMethod.GET)
	List<Adress> getEstablishmentAddressesWithName(@PathVariable String enterpriseName) {
		String entityNumber = enterpriseService.getEntityNumberWithName(enterpriseName);
		return getEstablishmentAddresses(entityNumber);
	}	

	/**
	 * Web service which post the changes in an address receiving a Address type<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/establishments/{entityNumber}/addresses <br>
	 * @param address	Object type Address with the updates
	 * @return	message with the result of the operation
	 */
	@RequestMapping(value = "/establishments/{entityNumber}/addresses", method = RequestMethod.POST, consumes = "application/json")
	public ServiceResponse updateAddress(@RequestBody Adress address) {
		boolean isUpdated = enterpriseService.updateAddress(address);
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	/**
	 * Web service which post the changes in an address receiving a a list of parameters and the entity number<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/establishments/{entityNumber}/addressesParams <br>
	 * @param entityNumber	String, number of the enterprise
	 * @param parameters List<AddressParams> list of objects AddressParams which have the parameter and value of change
	 * @return	message with the result of the operation
	 */
	@RequestMapping(value = "/establishments/{entityNumber}/addressesParams", method = RequestMethod.POST, consumes = "application/json")
	public ServiceResponse updateAddressParams(@PathVariable String entityNumber,@RequestBody List<AddressParams> parameters) {
		List<AddressParams> params = parameters;
		for (int i = 0; i < params.size(); i++){
			if (params.get(i).getParameter().equals("typeofaddress")){
				AddressParams addressP = params.get(i);
				addressP.setValue(enterpriseAllService.getLanguageDescription(addressP.getValue()).get(0).getCodeid());
				params.set(i, addressP);
			}
			
		}
		boolean isUpdated = enterpriseService.updateAddressParams(params, entityNumber);
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}

}