package com.uantwerp.project_databases.benchmark.ws;

import java.util.ArrayList;
import java.util.List;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.uantwerp.project_databases.benchmark.db2.entity.Adress;

import bpost_production.wsdl.AddressToValidateType;
import bpost_production.wsdl.DeliveryPointLocationComplexType;
import bpost_production.wsdl.PostalAddressType;
import bpost_production.wsdl.PostalCodeMunicipalityComplexType;
import bpost_production.wsdl.StructuredDeliveryPointLocationComplexType;
import bpost_production.wsdl.StructuredPostalCodeMunicipalityComplexType;
import bpost_production.wsdl.ValidateAddressesRequestType;
import bpost_production.wsdl.ValidateAddressesRequestType.AddressToValidateList;
import bpost_production.wsdl.ValidateAddressesResponseType;
import bpost_production.wsdl.ValidatedAddressResultType;

public class BPOSTAddressClient extends WebServiceGatewaySupport {

	public ValidatedAddressResultType validateAddress(Adress address) {
		ValidateAddressesRequestType request = new ValidateAddressesRequestType();
		AddressToValidateList validateList = new AddressToValidateList();
		AddressToValidateType addressToValidate = new AddressToValidateType();
		List<AddressToValidateType> addressToValidateList = new ArrayList<AddressToValidateType>();

		addressToValidate.setId("1");

		PostalAddressType postalAddressType = new PostalAddressType();
		PostalCodeMunicipalityComplexType postalCodeMunicipalityComplex = new PostalCodeMunicipalityComplexType();
		StructuredPostalCodeMunicipalityComplexType value = new StructuredPostalCodeMunicipalityComplexType();
		value.setMunicipalityName(address.getMunicipalitynl());
		value.setPostalCode(address.getZipCode());
		postalCodeMunicipalityComplex.setStructuredPostalCodeMunicipality(value);
		postalAddressType.setPostalCodeMunicipality(postalCodeMunicipalityComplex);

		DeliveryPointLocationComplexType deliveryPointLocation = new DeliveryPointLocationComplexType();
		StructuredDeliveryPointLocationComplexType structuredDeliveryPointLocation = new StructuredDeliveryPointLocationComplexType();
		structuredDeliveryPointLocation.setStreetName(address.getStreetnl());
		structuredDeliveryPointLocation.setStreetNumber(address.getHousenumber());
		structuredDeliveryPointLocation.setBoxNumber(address.getBox());
		deliveryPointLocation.setStructuredDeliveryPointLocation(structuredDeliveryPointLocation);
		postalAddressType.setDeliveryPointLocation(deliveryPointLocation);

		addressToValidate.setPostalAddress(postalAddressType);

		addressToValidateList.add(addressToValidate);
		// validateList.setAddressToValidate(addressToValidateList);
		request.setAddressToValidateList(validateList);

		ValidateAddressesResponseType response = (ValidateAddressesResponseType) getWebServiceTemplate()
				.marshalSendAndReceive(request);

		if (response.getValidatedAddressResultList() != null
				&& response.getValidatedAddressResultList().getValidatedAddressResult() != null)
			return response.getValidatedAddressResultList().getValidatedAddressResult().get(0);
		else
			return null;
	}
}
