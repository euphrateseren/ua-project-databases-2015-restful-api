package com.uantwerp.project_databases.benchmark.db2.service;

import java.util.List;

import org.apache.tomcat.jni.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uantwerp.project_databases.benchmark.db2.entity.Activity;
import com.uantwerp.project_databases.benchmark.db2.entity.Adress;
import com.uantwerp.project_databases.benchmark.db2.entity.Contact;
import com.uantwerp.project_databases.benchmark.db2.entity.Denomination;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;
import com.uantwerp.project_databases.benchmark.db2.entity.EnterpriseAll;
import com.uantwerp.project_databases.benchmark.db2.entity.EnterpriseReal;
import com.uantwerp.project_databases.benchmark.db2.entity.Languages;
import com.uantwerp.project_databases.benchmark.db2.repo.EnterpriseRepository;
import com.uantwerp.project_databases.benchmark.db2.repo.EnterpriseRepositoryAll;

@Service
public class EnterpriseAllService {

	@Autowired
	private EnterpriseRepositoryAll enterpriseAllRepository;

	public EnterpriseAll getEnterpriseInfo(String enterpriseNumber) {
		EnterpriseAll enterpriseList = enterpriseAllRepository.getEnterpriseInfoId(enterpriseNumber);
		return enterpriseList;
	}
	
	public EnterpriseAll getEnterpriseInfoName(String enterpriseName) {
		EnterpriseAll enterpriseList = enterpriseAllRepository.getEnterpriseInfoName(enterpriseName);
		return enterpriseList;
	}
	

	public List<Denomination> getDenominationLike(String enterpriseName){
		return enterpriseAllRepository.getDenominationLike(enterpriseName);
	}
	
	public List<Languages> getLanguageCodeId(String codeID){
		return enterpriseAllRepository.getLanguageCodeID(codeID);
	}
	
	public List<Languages> getLanguageDescription(String description){
		return enterpriseAllRepository.getLanguageDescription(description);
	}
	
	public boolean addContact(Contact contact){
		return enterpriseAllRepository.addContact(contact);
	}

	public boolean removeContact(Contact contact){
		return enterpriseAllRepository.removeContact(contact);
	}
	
	public boolean addContact(String entityNumber,String entityContact,String contactType,String value){
		return enterpriseAllRepository.addContact(entityNumber, entityContact, contactType, value);
	}
		
	public boolean removeContact(String entityNumber,String contactType,String entityContact){
		return enterpriseAllRepository.removeContact(entityNumber, contactType, entityContact);
	}
	
	public boolean addActivity(Activity activity){
		return enterpriseAllRepository.addActivity(activity);
	}
	
	public boolean removeActivity(Activity activity){
		return enterpriseAllRepository.removeActivity(activity);
	}
		
	public boolean addActivity(String entityNumber,String activityGroup,String naceVersion,String naceCode,String classification){
		return enterpriseAllRepository.addActivity(entityNumber, activityGroup, naceVersion, naceCode, classification);
	}

	public boolean removeActivity(String entityNumber,String activityGroup, String naceCode){
		return enterpriseAllRepository.removeActivity(entityNumber, activityGroup, naceCode);
	}

}
