package com.uantwerp.project_databases.benchmark.db2.entity;

public class Contact {
	private String entityNumber, entityContact, contactType, value;

	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Contact(String entityNumber, String entityContact, String contactType, String value) {
		super();
		this.entityNumber = entityNumber;
		this.entityContact = entityContact;
		this.contactType = contactType;
		this.value = value;
	}

	public String getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(String entityNumber) {
		this.entityNumber = entityNumber;
	}

	public String getEntityContact() {
		return entityContact;
	}

	public void setEntityContact(String entityContact) {
		this.entityContact = entityContact;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	
	
	
}
