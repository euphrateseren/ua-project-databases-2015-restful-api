package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.uantwerp.project_databases.benchmark.db2.entity.Adress;
//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Denomination;

@Repository("denominationRepository")
public class DenominationRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final RowMapper<Denomination> denominationRowMapper = new RowMapper<Denomination>() {
		@Override
		public Denomination mapRow(ResultSet rs, int rowNum) throws SQLException {
			Denomination denomination = new Denomination(rs.getString("ENTITYNUMBER"), rs.getString("LANGUAGE"),
					rs.getString("TYPEOFDENOMINATION"), rs.getString("DENOMINATION"));
			return denomination;
		}
	};

	public List<Denomination> selectDenominationEnterpriseNumber(String enterpriseNumber) {
		return jdbcTemplate.query("SELECT entitynumber, "
								+ "(SELECT description From schdbenterp.languages where code_id = language and language = 'NL')language, "
								+ "(SELECT description From schdbenterp.languages where code_id = typeofdenomination and language = 'NL') typeofdenomination, "
								+ "denomination"+
									" FROM schdbenterp.denomination"+
									" Where entitynumber = '"+enterpriseNumber+"'" ,
		denominationRowMapper);
	}
	
	public boolean updateDenomination(Denomination denomination) {
		String updateQuery = "update schdbenterp.address set  language = ?, "
				+ "typeofdenomination = ?, denomination = ? "
				+ "where entityNumber = ?";
		int row = jdbcTemplate.update(updateQuery, denomination.getLanguage(), denomination.getTypeOfDenomination(),
				denomination.getDenomination());
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean updateCompanyName(String entityNumber, String typeOfDenomination, String denomination) {
		String updateQuery = "update schdbenterp.address set "
				+ "denomination = ? "
				+ "where typeofdenomination= ? and entityNumber = ?";
		int row = jdbcTemplate.update(updateQuery, denomination, typeOfDenomination,
				entityNumber);
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public List<Denomination> selectDenominationEnterpriseName(String enterpriseName) {
		return jdbcTemplate.query("SELECT entitynumber, "
								+ "(SELECT description From schdbenterp.languages where code_id = language and language = 'NL')language, "
								+ "(SELECT description From schdbenterp.languages where code_id = typeofdenomination and language = 'NL') typeofdenomination, "
								+ "denomination"+
									" FROM schdbenterp.denomination"+
									" Where denomination = '"+enterpriseName+"'" ,
		denominationRowMapper);
	}
	
	public List<Denomination> selectDenominationEnterpriseNameLike(String enterpriseName) {
		return jdbcTemplate.query("SELECT entitynumber, "
					+ "(SELECT description From schdbenterp.languages where code_id = language and language = 'NL')language, "
								+ "(SELECT description From schdbenterp.languages where code_id = typeofdenomination and language = 'NL') typeofdenomination, "
					+ " denomination"+
									" FROM schdbenterp.denomination"+
									" Where denomination like '"+enterpriseName+"%'" ,
		denominationRowMapper);
	}

}
