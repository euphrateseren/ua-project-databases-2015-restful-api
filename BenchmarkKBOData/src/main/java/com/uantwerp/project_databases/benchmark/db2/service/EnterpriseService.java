package com.uantwerp.project_databases.benchmark.db2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uantwerp.project_databases.benchmark.db2.entity.Adress;
import com.uantwerp.project_databases.benchmark.db2.entity.Denomination;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;
import com.uantwerp.project_databases.benchmark.db2.repo.AddressRepository;
import com.uantwerp.project_databases.benchmark.db2.repo.DenominationRepository;
import com.uantwerp.project_databases.benchmark.db2.repo.EnterpriseRepository;
import com.uantwerp.project_databases.benchmark.db2.repo.EstablishmentAddress;
import com.uantwerp.project_databases.benchmark.db2.repo.EstablishmentRepository;
import com.uantwerp.project_databases.benchmark.utilities.AddressParams;

@Service
public class EnterpriseService {

	@Autowired
	private EstablishmentRepository establishmentRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private EnterpriseRepository enterpriseRepository;

	@Autowired
	private DenominationRepository denominationRepository;

	public Enterprise getEnterprise(long enterpriseNumber) {
		List<Enterprise> enterpriseList = enterpriseRepository.selectQueryWithEnterpriseNumber(enterpriseNumber);
		if (enterpriseList != null && enterpriseList.isEmpty() == false)
			return enterpriseList.get(0);
		else
			return null;
	}

	public Enterprise getNextEnterprise(long enterpriseNumber) {
		List<Enterprise> enterpriseList = enterpriseRepository.selectQueryNextEnterpriseNumber(enterpriseNumber);
		if (enterpriseList != null && enterpriseList.isEmpty() == false)
			return enterpriseList.get(0);
		else
			return null;
	}

	public EstablishmentAddress getEstablishmentCountAndAddress(String enterpriseNumber) {
		int establishmentCount = establishmentRepository.getEstablishmentCount(enterpriseNumber);
		List<Adress> addresses = addressRepository.getAddress(enterpriseNumber);
		EstablishmentAddress establishmentAddress = new EstablishmentAddress(establishmentCount, addresses);
		return establishmentAddress;
	}

	public int getEstablishmentCount(String enterpriseNumber) {
		int establishmentCount = establishmentRepository.getEstablishmentCount(enterpriseNumber);
		return establishmentCount;
	}

	public List<Adress> getEstablishmentAddresses(String enterpriseNumber) {
		List<Adress> addresses = addressRepository.getAddress(enterpriseNumber);
		return addresses;
	}

	public String getEntityNumberWithName(String enterpriseName) {
		List<Denomination> list = denominationRepository.selectDenominationEnterpriseName(enterpriseName);
		if (list.isEmpty())
			return null;
		else
			return list.get(0).getEntityNumber();
	}
	
	public boolean updateAddress(Adress address) {
		return addressRepository.updateAddress(address);
	}
	
	public boolean updateAddressParams(List<AddressParams> parameters, String entityNumber){
		return addressRepository.updateAddressParams(parameters, entityNumber);
	}
}
