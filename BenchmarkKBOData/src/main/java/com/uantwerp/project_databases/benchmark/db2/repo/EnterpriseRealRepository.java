package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Contact;
import com.uantwerp.project_databases.benchmark.db2.entity.Denomination;
import com.uantwerp.project_databases.benchmark.db2.entity.EnterpriseReal;

@Repository("enterpriseRealRepository")
public class EnterpriseRealRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final RowMapper<EnterpriseReal> enterpiseRealRowMapper = new RowMapper<EnterpriseReal>() {
		@Override
		public EnterpriseReal mapRow(ResultSet rs, int rowNum) throws SQLException {
			EnterpriseReal enterpriseReal = new EnterpriseReal(rs.getString("ENTERPRISENUMBER"), rs.getString("STATUS"),
					rs.getString("JURIDICALSITUATION"), rs.getString("TYPEOFENTERPRISE"), rs.getString("JURIDICALFORM")
					, rs.getString("STARTDATE"));
			return enterpriseReal;
		}
	};

	public List<EnterpriseReal> selectEnterprise(String enterpriseNumber) {
		return jdbcTemplate.query("SELECT ENTERPRISENUMBER, "
							+ "(SELECT description From schdbenterp.languages where code_id = STATUS and language = 'NL') STATUS, "
							+ "(SELECT description From schdbenterp.languages where code_id = JURIDICALSITUATION and language = 'NL') JURIDICALSITUATION, "
							+ "(SELECT description From schdbenterp.languages where code_id = TYPEOFENTERPRISE and language = 'NL') TYPEOFENTERPRISE, "
							+ "(SELECT description From schdbenterp.languages where code_id = JURIDICALFORM and language = 'NL') JURIDICALFORM, "
							+ "STARTDATE "+
									" FROM schdbenterp.enterprise"+
									" Where enterprisenumber = '"+enterpriseNumber+"'" ,
									enterpiseRealRowMapper);
	}
	
	public boolean updateEnterprise(EnterpriseReal enterpriseReal) {
		String updateQuery = "update schdbenterp.enterprise set  STATUS = ?, "
				+ "JURIDICALSITUATION = ?, TYPEOFENTERPRISE = ?, JURIDICALFORM = ?, STARTDATE = ? "
				+ "where entityNumber = ?";
		int row = jdbcTemplate.update(updateQuery, enterpriseReal.getStatus(), enterpriseReal.getJuridicalSituation(),
				enterpriseReal.getTypeOfEnteerprise(),enterpriseReal.getJuridicalForm(),enterpriseReal.getStartDate());
		
		if (row > 0)
			return true;
		else
			return false;
	}
	

}
