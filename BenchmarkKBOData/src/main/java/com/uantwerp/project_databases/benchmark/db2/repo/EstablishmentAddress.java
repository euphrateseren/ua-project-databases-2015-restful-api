package com.uantwerp.project_databases.benchmark.db2.repo;

import java.util.List;

import com.uantwerp.project_databases.benchmark.db2.entity.Adress;

public class EstablishmentAddress {
	private int establishmentCount;
	private List<Adress> addresses;

	public int getEstablishmentCount() {
		return establishmentCount;
	}

	public EstablishmentAddress(int establishmentCount, List<Adress> addresses) {
		super();
		this.establishmentCount = establishmentCount;
		this.addresses = addresses;
	}

	public void setEstablishmentCount(int establishmentCount) {
		this.establishmentCount = establishmentCount;
	}

	public List<Adress> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Adress> addresses) {
		this.addresses = addresses;
	}

}
