package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.uantwerp.project_databases.benchmark.db2.entity.Activity;
//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Contact;

@Repository("contactRepository")
public class ContactRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final RowMapper<Contact> contactRowMapper = new RowMapper<Contact>() {
		@Override
		public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
			Contact contact = new Contact(rs.getString("ENTITYNUMBER"), rs.getString("ENTITYCONTACT"),
					rs.getString("CONTACTTYPE"), rs.getString("VALUE"));
			return contact;
		}
	};

	public List<Contact> selectContactEnterpriseNumber(String enterpriseNumber) {
		return jdbcTemplate.query("SELECT entitynumber, "
							+ "(SELECT description From schdbenterp.languages where code_id = entitycontact and language = 'NL') entitycontact, "
							+ "(SELECT description From schdbenterp.languages where code_id = contacttype and language = 'NL') contacttype, "
							+ "value"+
									" FROM schdbenterp.contact"+
									" Where entitynumber = '"+enterpriseNumber+"'" ,
				contactRowMapper);
	}
	
	public boolean updateContact(Contact contact) {
		String updateQuery = "update schdbenterp.contact set  entitycontact = ?, "
				+ "contacttype = ?, value = ? "
				+ "where entityNumber = ?";
		int row = jdbcTemplate.update(updateQuery, contact.getEntityContact(), contact.getContactType(),
				contact.getValue());
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean addContact(String entityNumber, String entityContact, String contactType, String value){
		String updateQuery = "insert into schdbenterp.contact(entitynumber, entityContact, contactType, value) "
				+ " values( ?,?,?,?)";
		int row = jdbcTemplate.update(updateQuery, entityNumber, entityContact,contactType,value);
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean reemoveContact(String entityNumber, String contactType, String entityContact){
		String updateQuery = "delete from schdbenterp.contact "
				+ " where entityNumber = ? and contacttype = ?"
				+ " and entityContact = ?";
		int row = jdbcTemplate.update(updateQuery, entityNumber, contactType, entityContact);
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean reemoveContact(Contact contact){
		String updateQuery = "delete from schdbenterp.contact "
				+ " where entityNumber = ? and contacttype = ?"
				+ " and entityContact = ?";
		int row = jdbcTemplate.update(updateQuery, contact.getEntityNumber(), contact.getContactType(), contact.getEntityContact());
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean addContact(Contact contact){
		String updateQuery = "insert into schdbenterp.contact(entitynumber, entityContact, contactType, value) "
				+ "values( ?,?,?,?)";
		int row = jdbcTemplate.update(updateQuery, contact.getEntityNumber(), contact.getEntityContact(),contact.getContactType(),contact.getValue());
		
		if (row > 0)
			return true;
		else
			return false;
	}

}
