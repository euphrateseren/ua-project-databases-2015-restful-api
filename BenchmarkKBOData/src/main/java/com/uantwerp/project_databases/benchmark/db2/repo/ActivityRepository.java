package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Activity;
import com.uantwerp.project_databases.benchmark.db2.entity.Adress;
import com.uantwerp.project_databases.benchmark.db2.entity.Contact;

@Repository("activityRepository")
public class ActivityRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final RowMapper<Activity> activityRowMapper = new RowMapper<Activity>() {
		@Override
		public Activity mapRow(ResultSet rs, int rowNum) throws SQLException {
			Activity activity = new Activity(rs.getString("ENTITYNUMBER"), rs.getString("ACTIVITYGROUP"), rs.getString("CLASSIFICATION"),
					rs.getString("NACEVERSION"), rs.getString("NACECODE"));
			return activity;
		}
	};
	
	public List<Activity> selectActivityEnterpriseNumber(String enterpriseNumber) {
		return jdbcTemplate.query("SELECT entitynumber, "
								+ "(SELECT description From schdbenterp.languages where code_id = ACTIVITYGROUP and language = 'NL') ACTIVITYGROUP, "
								+ "(SELECT description From schdbenterp.languages where code_id = CLASSIFICATION and language = 'NL') CLASSIFICATION, "
								+ "(SELECT description From schdbenterp.languages where code_id = NACEVERSION and language = 'NL') NACEVERSION, "
								+ "NACECODE "
								+
									" FROM schdbenterp.activity"+
									" Where entitynumber = '"+enterpriseNumber+"'" ,
									activityRowMapper);
	}
	
	public boolean updateActivity(Activity activity) {
		String updateQuery = "update schdbenterp.activity set  activitygroup = ?, "
				+ "naceversion = ?, NACECODE = ?, CLASSIFICATION = ? "
				+ "where entityNumber = ?";
		int row = jdbcTemplate.update(updateQuery, activity.getActivityGroup(), activity.getNaceVersion(),
				activity.getNaceCode(), activity.getClassification());
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public List<Activity> findByActivityGroup(String activityGroup) {
		return jdbcTemplate.query("SELECT ENTITYNUMBER, ACTIVITYGROUP, NACEVERSION, NACECODE, CLASSIFICATION FROM SCHDBENTERP.ACTIVITY WHERE ACTIVITYGROUP='" + activityGroup + "'",
				activityRowMapper);
	}
	
	public void InsertAllData(long numData){
		for (int i = 0; i <  numData;i++){
			this.jdbcTemplate.update(
			        "insert into SCHDBENTERP.TESTDBINSERT (CNUMBER, CTEXT) values (?, ?)",
			        i, "Text " + i);
		}
	}
	
	public void InsertResults(String test, int NumIteration, long time, String typeOfTest){
		this.jdbcTemplate.update(
		        "insert into SCHDBENTERP.RESULTTESTS (TEST, ITERATION, RESTULT_TIME, TYPE) values (?, ?, ?, ?)",
		        test, NumIteration, time, typeOfTest);
	}
	
	public List<Activity> SelectAllData(){
		return jdbcTemplate.query("SELECT ENTITYNUMBER, ACTIVITYGROUP, NACEVERSION, NACECODE, CLASSIFICATION FROM SCHDBENTERP.ACTIVITY",
				activityRowMapper);
	}
	
	public void InsertOne(long dataToInsert){
		this.jdbcTemplate.update(
		        "insert into SCHDBENTERP.TESTDBINSERT (CNUMBER, CTEXT) values (?, ?)",
		        dataToInsert, "Text " + dataToInsert);
	}
	
	public void UpdateOne(long dataToUpdate){
		this.jdbcTemplate.update(
		        "update SCHDBENTERP.TESTDBINSERT set CTEXT = CTEXT || ' UPDATED' WHERE CNUMBER=?",
		        dataToUpdate);
	}
	
	public void UpdateAllData(){
		this.jdbcTemplate.update(
		        "update SCHDBENTERP.TESTDBINSERT set CTEXT = CTEXT || ' UPDATED'");
	}
	
	public void DeleteOne(long dataToDelete){
		this.jdbcTemplate.update(
		        "delete from SCHDBENTERP.TESTDBINSERT where CNUMBER = ?",
		        dataToDelete);
	}
	
	public void DeleteAllData(){
		this.jdbcTemplate.update(
		        "delete from SCHDBENTERP.TESTDBINSERT");
	}
	
	public boolean addActivity(String entityNumber, String activityGroup, String naceVersion, String naceCode, String classification){
		String updateQuery = "insert into schdbenterp.activity(entitynumber, activityGroup, naceVersion, naceCode, classification) "
				+ " values( ?,?,?,?,?)";
		int row = jdbcTemplate.update(updateQuery, entityNumber, activityGroup, naceVersion, naceCode, classification);
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean removeActivity(String entityNumber, String activityGroup, String naceCode){
		String updateQuery = "delete from schdbenterp.activity "
				+ " where entityNumber = ? "
				+ " and activityGroup = ? and naceCode = ?";
		int row = jdbcTemplate.update(updateQuery, entityNumber, activityGroup, naceCode);
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean removeActivity(Activity activity){
		String updateQuery = "delete from schdbenterp.activity "
				+ " where entityNumber = ? and activityGroup = ?"
				+ " and classification = ? and naceCode = ?";
		int row = jdbcTemplate.update(updateQuery, 
										activity.getEntityNumber(), 
										activity.getActivityGroup(), 
										activity.getClassification(),
										activity.getNaceCode());
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean addActivity(Activity activity){
		String updateQuery = "insert into schdbenterp.activity(entitynumber, activityGroup, classification, naceVersion, naceCode) "
				+ "values( ?,?,?,?,?)";
		int row = jdbcTemplate.update(updateQuery, 
									activity.getEntityNumber(), 
									activity.getActivityGroup(),
									activity.getClassification(),
									activity.getNaceVersion(),
									activity.getNaceCode());
		
		if (row > 0)
			return true;
		else
			return false;
	}
	

}
