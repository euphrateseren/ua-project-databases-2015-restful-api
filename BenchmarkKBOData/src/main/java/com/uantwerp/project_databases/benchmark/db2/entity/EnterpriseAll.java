package com.uantwerp.project_databases.benchmark.db2.entity;

import java.util.List;

public class EnterpriseAll {
	private List<Denomination> socialInformation;
	private EnterpriseReal enterprise;
	private List<Activity> activity;
	private List<Contact> contacts;
	private Adress address;
	
	public EnterpriseAll() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EnterpriseAll(List<Denomination> socialInformation, EnterpriseReal enterprise, List<Activity> activity,
			List<Contact> contacts, Adress address) {
		super();
		this.socialInformation = socialInformation;
		this.enterprise = enterprise;
		this.activity = activity;
		this.contacts = contacts;
		this.address = address;
	}

	public List<Denomination> getSocialInformation() {
		return socialInformation;
	}

	public void setSocialInformation(List<Denomination> socialInformation) {
		this.socialInformation = socialInformation;
	}

	public EnterpriseReal getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(EnterpriseReal enterprise) {
		this.enterprise = enterprise;
	}

	public List<Activity> getActivity() {
		return activity;
	}

	public void setActivity(List<Activity> activity) {
		this.activity = activity;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public Adress getAddress() {
		return address;
	}

	public void setAddress(Adress address) {
		this.address = address;
	}

	
	
}
