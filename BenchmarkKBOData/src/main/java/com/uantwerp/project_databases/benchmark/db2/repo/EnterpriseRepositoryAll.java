package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.uantwerp.project_databases.benchmark.db2.entity.Activity;
import com.uantwerp.project_databases.benchmark.db2.entity.Adress;
import com.uantwerp.project_databases.benchmark.db2.entity.Contact;
import com.uantwerp.project_databases.benchmark.db2.entity.Denomination;
//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.EnterpriseAll;
import com.uantwerp.project_databases.benchmark.db2.entity.EnterpriseReal;
import com.uantwerp.project_databases.benchmark.db2.entity.Languages;
import com.uantwerp.project_databases.benchmark.db2.repo.*;

@Repository("enterpriseAllRepository")
public class EnterpriseRepositoryAll {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private EnterpriseRealRepository enterpriseRealRepository;
	@Autowired
	private AddressRepository addressRepository;
	@Autowired
	private ContactRepository contactRepository;
	@Autowired
	private DenominationRepository denominationRepository;
	@Autowired
	private ActivityRepository activityRepository;
	@Autowired
	private LanguagesRepository languagesRepository;

	public EnterpriseAll getEnterpriseInfoId(String enterpriseNumber){
		List<EnterpriseReal> enterprise = enterpriseRealRepository.selectEnterprise(enterpriseNumber);
		if (enterprise == null && enterprise.isEmpty() == true)
			enterprise = null;
		List<Adress> address = addressRepository.getAddress(enterpriseNumber);
		if (address == null && address.isEmpty() == true)
			address = null;
		List<Contact> contacts = contactRepository.selectContactEnterpriseNumber(enterpriseNumber);
		if (contacts == null && contacts.isEmpty() == true)
			contacts = null;
		List<Denomination> denomination = denominationRepository.selectDenominationEnterpriseNumber(enterpriseNumber);
		if (denomination == null && denomination.isEmpty() == true)
			denomination = null;
		List<Activity> activity = activityRepository.selectActivityEnterpriseNumber(enterpriseNumber);
		if (activity == null && activity.isEmpty() == true)
			activity = null;
		EnterpriseAll enterp =  new EnterpriseAll(denomination, enterprise.get(0), activity, contacts, address.get(0));
		return enterp;
		
	}
	
	public EnterpriseAll getEnterpriseInfoName(String enterpriseName){
		List<Denomination> denomination = denominationRepository.selectDenominationEnterpriseName(enterpriseName);
		if (denomination == null && denomination.isEmpty() == true)
			denomination = null;
		if (denomination != null){
			String enterpriseNumber = denomination.get(0).getEntityNumber();
			List<EnterpriseReal> enterprise = enterpriseRealRepository.selectEnterprise(enterpriseNumber);
			if (enterprise == null && enterprise.isEmpty() == true)
				enterprise = null;
			List<Adress> address = addressRepository.getAddress(enterpriseNumber);
			if (address == null && address.isEmpty() == true)
				address = null;
			List<Contact> contacts = contactRepository.selectContactEnterpriseNumber(enterpriseNumber);
			if (contacts == null && contacts.isEmpty() == true)
				contacts = null;
			List<Activity> activity = activityRepository.selectActivityEnterpriseNumber(enterpriseNumber);
			if (activity == null && activity.isEmpty() == true)
				activity = null;
			EnterpriseAll enterp =  new EnterpriseAll(denomination, enterprise.get(0), activity, contacts, address.get(0));
			return enterp;
		}
		return null;
	}
	
	public List<Denomination> getDenominationLike(String enterpriseName){
		return denominationRepository.selectDenominationEnterpriseNameLike(enterpriseName);
	}
	
	public List<Languages> getLanguageCodeID(String codeID){
		return languagesRepository.selectLanguageCodeID(codeID);
	}
	
	public List<Languages> getLanguageDescription(String description){
		return languagesRepository.selectLanguageDescription(description);
	}

	public boolean addContact(Contact contact){
		return contactRepository.addContact(contact);
	}
	
	public boolean addContact(String entityNumber,String entityContact,String contactType,String value){
		return contactRepository.addContact(entityNumber, entityContact, contactType, value);
	}
	
	public boolean removeContact(Contact contact){
		return contactRepository.reemoveContact(contact);
	}
	
	public boolean removeContact(String entityNumber,String contactType,String entityContact){
		return contactRepository.reemoveContact(entityNumber, contactType, entityContact);
	}
	
	public boolean addActivity(Activity activity){
		return activityRepository.addActivity(activity);
	}
	
	public boolean addActivity(String entityNumber,String activityGroup,String naceVersion,String naceCode,String classification){
		return activityRepository.addActivity(entityNumber, activityGroup, naceVersion, naceCode, classification);
	}
	
	public boolean removeActivity(Activity activity){
		return activityRepository.removeActivity(activity);
	}
	
	public boolean removeActivity(String entityNumber, String activityGroup, String naceCode){
		return activityRepository.removeActivity(entityNumber, activityGroup,naceCode);
	}

}
