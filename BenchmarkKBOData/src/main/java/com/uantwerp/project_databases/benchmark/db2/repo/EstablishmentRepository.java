package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;

@Repository
public class EstablishmentRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	// private static final RowMapper<Enterprise> queryRowMapper = new
	// RowMapper<Enterprise>() {
	// @Override
	// public Enterprise mapRow(ResultSet rs, int rowNum) throws SQLException {
	// Enterprise enterprise = new Enterprise(rs.getLong("ENTERPRISENUMBER"),
	// rs.getString("DENOMINATION"),
	// rs.getString("CONTACT_WEB"),rs.getString("CONTACT_MAIL"));
	// return enterprise;
	// }
	// };

	public int getEstablishmentCount(String entityNumber) {
		return jdbcTemplate
				.queryForInt("select count(*) from schdbenterp.establishment where entityNumber='" + entityNumber + "'");
	}
}
