package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;

@Repository("enterpriseRepository")
public class EnterpriseRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	private static final RowMapper<Enterprise> queryRowMapper = new RowMapper<Enterprise>() {
		@Override
		public Enterprise mapRow(ResultSet rs, int rowNum) throws SQLException {
			Enterprise enterprise = new Enterprise(rs.getString("ENTERPRISENUMBER"), rs.getString("DENOMINATION"),
					rs.getString("CONTACT_WEB"),rs.getString("CONTACT_MAIL"));
			return enterprise;
		}
	};

	public List<Enterprise> selectQueryWithEnterpriseNumber(long enterpriseNumber) {
		return jdbcTemplate.query("SELECT a.enterprisenumber enterprisenumber, "
				+ " b.denomination,"
				         +" (SELECT value"
				         +" FROM schdbenterp.contact c"
				         +" WHERE c.entitynumber = a.enterprisenumber"
				         +" AND      c.contacttype = 'ContactType-WEB') CONTACT_WEB,"
				+" (SELECT value"
				          +" FROM schdbenterp.contact c"
				         +" WHERE c.entitynumber = a.enterprisenumber"
				         +" AND      c.contacttype = 'ContactType-EMAIL') CONTACT_MAIL"
				+" FROM schdbenterp.enterprise  a, schdbenterp.denomination b"
				+" WHERE to_number(replace(a.enterprisenumber,'.','')) = "+enterpriseNumber
				+" AND a.enterprisenumber = b.entitynumber"
				+" AND b.typeofdenomination = 'TypeOfDenomination-001'",
				queryRowMapper);
	}
	
	public List<Enterprise> selectQueryNextEnterpriseNumber(long enterpriseNumber) {
		return jdbcTemplate.query(" SELECT a.enterprisenumber enterprisenumber," 
								          +" b.denomination,"
								         +" (SELECT value"
								          +" FROM schdbenterp.contact c"
								         +" WHERE c.entitynumber = a.enterprisenumber"
								         +" AND      c.contacttype = 'ContactType-WEB') CONTACT_WEB,"
								+" (SELECT value"
								          +" FROM schdbenterp.contact c"
								         +" WHERE c.entitynumber = a.enterprisenumber"
								         +" AND      c.contacttype = 'ContactType-EMAIL') CONTACT_MAIL"
								+" FROM schdbenterp.enterprise  a, schdbenterp.denomination b"
								+" WHERE to_number(replace(a.enterprisenumber,'.','')) >"+enterpriseNumber
								+" AND a.enterprisenumber = b.entitynumber"
								+" AND b.typeofdenomination = 'TypeOfDenomination-001'"
								+" AND	 to_number(replace(enterprisenumber,'.','')) = (SELECT min(to_number(replace(enterprisenumber,'.','')))" 
								+" from schdbenterp.enterprise "
								+" where to_number(replace(enterprisenumber,'.','')) > "+enterpriseNumber+")"
,
				queryRowMapper);
	}
}
