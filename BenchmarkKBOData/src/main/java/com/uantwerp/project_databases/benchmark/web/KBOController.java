package com.uantwerp.project_databases.benchmark.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.uantwerp.project_databases.benchmark.utilities.Utility;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;
import com.uantwerp.project_databases.benchmark.db2.service.EnterpriseService;

@RestController
@RequestMapping(value = "kbo_data/api/v1")
/** Class that maps the web services related to the other group necessities 
*
* @author gerito
* @version 1.0 2015
*/
public class KBOController extends BaseController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(KBOController.class);

	@Autowired
	EnterpriseService enterpriseService;

	/**
	 * Get the social information of an enterprise given a enterprise number <br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/enterprise/{enterpriseNumber} <br>
	 * @param enterpriseNumber	String with the enterprise number of the enterprise
	 * @return	JASON with social information such as web and mail
	 */
	@RequestMapping(value = "/enterprise/{enterpriseNumber}", method = RequestMethod.GET)
	Enterprise getEnterprise(@PathVariable int enterpriseNumber) {
		return enterpriseService.getEnterprise(enterpriseNumber);
	}

	/**
	 * Get the social information of the next enterprise given a enterprise number <br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/enterprise/next/{enterpriseNumber} <br>
	 * @param enterpriseNumber	String with the enterprise number of the enterprise
	 * @return	JASON with social information such as web and mail
	 */
	@RequestMapping(value = "/enterprise/next/{enterpriseNumber}", method = RequestMethod.GET)
	Enterprise getNextEnterprise(@PathVariable int enterpriseNumber) {
		return enterpriseService.getNextEnterprise(enterpriseNumber);
	}

}