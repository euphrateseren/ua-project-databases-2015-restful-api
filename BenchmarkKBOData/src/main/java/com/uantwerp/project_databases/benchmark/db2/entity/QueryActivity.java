package com.uantwerp.project_databases.benchmark.db2.entity;

public class QueryActivity {
	private String entityNumber, activity, classification;

	public QueryActivity() {
	}

	public QueryActivity(String entityNumber, String activity, String classification) {
		super();
		this.entityNumber = entityNumber;
		this.activity = activity;
		this.classification = classification;
	}

	public String getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(String entityNumber) {
		this.entityNumber = entityNumber;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

}
