package com.uantwerp.project_databases.benchmark.db2.entity;

public class Denomination {
	private String entityNumber, typeOfDenomination, language, denomination;

	public Denomination() {
	}

	public Denomination(String entityNumber, String language, String typeOfDenomination,  String denomination) {
		super();
		this.entityNumber = entityNumber;
		this.typeOfDenomination = typeOfDenomination;
		this.language = language;
		this.denomination = denomination;
	}

	public String getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(String entityNumber) {
		this.entityNumber = entityNumber;
	}

	public String getTypeOfDenomination() {
		return typeOfDenomination;
	}

	public void setTypeOfDenomination(String typeOfDenomination) {
		this.typeOfDenomination = typeOfDenomination;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}
	
	
}
