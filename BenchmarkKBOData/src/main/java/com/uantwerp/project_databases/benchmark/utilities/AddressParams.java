package com.uantwerp.project_databases.benchmark.utilities;

public class AddressParams {

	public String parameter;
	public String value;
	
	public AddressParams() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AddressParams(String parameter, String value) {
		super();
		this.parameter = parameter;
		this.value = value;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
