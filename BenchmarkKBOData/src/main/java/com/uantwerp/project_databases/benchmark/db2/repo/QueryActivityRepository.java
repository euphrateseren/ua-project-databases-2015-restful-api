package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.QueryActivity;

@Repository("queryActivityRepository")
public class QueryActivityRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final RowMapper<QueryActivity> queryRowMapper = new RowMapper<QueryActivity>() {
		@Override
		public QueryActivity mapRow(ResultSet rs, int rowNum) throws SQLException {
			QueryActivity activity = new QueryActivity(rs.getString("ENTITYNUMBER"), rs.getString("ACTIVITY"),
					rs.getString("CLASSIFICATION"));
			return activity;
		}
	};

	public List<QueryActivity> SelectQuery() {
		return jdbcTemplate.query("SELECT A.ENTERPRISENUMBER ENTITYNUMBER, C.DESCRIPTION ACTIVITY, D.DESCRIPTION CLASSIFICATION "
								+ "FROM SCHDBENTERP.ENTERPRISE A, SCHDBENTERP.ACTIVITY B, SCHDBENTERP.LANGUAGES C, SCHDBENTERP.LANGUAGES D "
								+ "WHERE A.ENTERPRISENUMBER = B.ENTITYNUMBER "
								+ "AND  B.CLASSIFICATION = C.CODE_ID "
								+ "AND B.ACTIVITYGROUP = D.CODE_ID "
								+ "AND C.LANGUAGE = 'NL' AND D.LANGUAGE = 'NL' "
								+ "AND A.ENTERPRISENUMBER = '0413.814.569'",
				queryRowMapper);
	}
}
