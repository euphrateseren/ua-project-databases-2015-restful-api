package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.uantwerp.project_databases.benchmark.db2.entity.Adress;
//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;
import com.uantwerp.project_databases.benchmark.utilities.AddressParams;

@Repository
public class AddressRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final RowMapper<Adress> queryRowMapper = new RowMapper<Adress>() {
		@Override
		public Adress mapRow(ResultSet rs, int rowNum) throws SQLException {
			Adress enterprise = new Adress(rs.getString("entitynumber"), rs.getString("typeofaddress"),
					rs.getString("countrynl"), rs.getString("municipalitynl"), rs.getString("streetnl"),
					rs.getString("housenumber"), rs.getString("extraaddressinfo"), rs.getString("zipCode"),
					rs.getString("box"));
			return enterprise;
		}
	};

	public List<Adress> getAddress(String entityNumber) {
		return jdbcTemplate.query("select entitynumber, "
								+ "(SELECT description From schdbenterp.languages where code_id = typeofaddress and language = 'NL') typeofaddress,"
								+ "countrynl,municipalitynl,streetnl,housenumber,extraaddressinfo,zipcode,box "
								+ "from schdbenterp.address "
								+ "where entitynumber='" + entityNumber + "'",
				queryRowMapper);
	}

	public boolean updateAddress(Adress address) {
		String updateQuery = "update schdbenterp.address set  typeofaddress = ?, "
				+ "countrynl = ?, municipalitynl = ?, streetnl = ?, housenumber = ?, extraaddressinfo = ? "
				+ "where entityNumber = ?";
		int row = jdbcTemplate.update(updateQuery, address.getTypeofaddress(), address.getCountrynl(),
				address.getMunicipalitynl(), address.getStreetnl(), address.getHousenumber(),
				address.getExtraaddressinfo(), address.getEntityNumber());
		
		if (row > 0)
			return true;
		else
			return false;
	}
	
	public boolean updateAddressParams(List<AddressParams> params, String entityNumber){
		String query = "update schdbenterp.address set ";
		for (int i = 0; i < params.size(); i++) {
			query =  query + params.get(i).getParameter() + " = '" +params.get(i).getValue()+"' ";
			if (i < params.size()-1){
				query = query + ", ";
			}
		}
		query = query +" where entitynumber = '"+ entityNumber+"'";
		System.out.println(query);
		int row = jdbcTemplate.update(query);
		if (row > 0)
			return true;
		else
			return false;
	}

}
