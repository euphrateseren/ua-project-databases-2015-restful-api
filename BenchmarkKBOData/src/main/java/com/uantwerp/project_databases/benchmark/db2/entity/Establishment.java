package com.uantwerp.project_databases.benchmark.db2.entity;

public class Establishment {
	private String establishmentnumber, startdate;

	public Establishment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Establishment(String establishmentnumber, String startdate) {
		super();
		this.establishmentnumber = establishmentnumber;
		this.startdate = startdate;
	}

	public String getEstablishmentnumber() {
		return establishmentnumber;
	}

	public void setEstablishmentnumber(String establishmentnumber) {
		this.establishmentnumber = establishmentnumber;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	
	
}
