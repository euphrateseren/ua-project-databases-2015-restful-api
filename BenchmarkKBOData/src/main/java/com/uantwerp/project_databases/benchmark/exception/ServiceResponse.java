package com.uantwerp.project_databases.benchmark.exception;

import java.io.Serializable;

/**
 * Detailed Exception Message response
 * 
 * @author aylin.gurkan,fatih.isler,firat.eren
 *
 */
public class ServiceResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private int errorCode;

	private String message;

	/**
	 * Default Constructor
	 */
	public ServiceResponse() {
	}

	/**
	 * Constructor
	 * 
	 * @param errorCode		Code of the error
	 * @param msg			Message with the error
	 */
	public ServiceResponse(int errorCode, String msg) {
		this.setMessage(msg);
		this.setErrorCode(errorCode);
	}

	/**
	 * Constructor
	 * 
	 * @param error		ResponseCodes with the error
	 */
	public ServiceResponse(ResponseCodes error) {
		this.message = error.message;
		this.errorCode = error.errorCode;
	}

	/**
	 * Constructor Converts ServiceRuntimeException to ServiceError
	 * 
	 * @param e		Exception of the service
	 */
	public ServiceResponse(ServiceRuntimeException e) {
		this.errorCode = e.getErrCode();
		this.message = e.getErrMsg();
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param errorCode
	 *            the errorCode to set
	 */
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

}
