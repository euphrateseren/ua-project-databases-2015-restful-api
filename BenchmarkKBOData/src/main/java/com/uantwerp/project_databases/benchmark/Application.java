package com.uantwerp.project_databases.benchmark;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring boot application context loader
 */
@SpringBootApplication
//@EnableJpaRepositories(basePackages = "com.uantwerp.project_databases.benchmark.db2.repo")
//@EntityScan(basePackages = "com.uantwerp.project_databases.benchmark.entity")
@EnableAutoConfiguration
//@EnableMongoRepositories
@ComponentScan
// @EnableScheduling
// @EnableAsync
// @EnableTransactionManagement
/**
 * 
 * @author gerito
 *
 */
public class Application implements CommandLineRunner {

	// public static void main(String[] args) {
	// SpringApplication.run(Application.class, args);
	// }
	//
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Application.class, "--debug");

		System.out.println("Let's inspect the beans provided by Spring Boot:");

		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			System.out.println(beanName);
		}
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("PRM_SURVEY_JOB started");

		// System.out.println("host: " + host);
		// MimeMessage message = javaMailSender.createMimeMessage();
		// System.out.println(javaMailSender.toString() + ":" +
		// message.toString());
	}

}

