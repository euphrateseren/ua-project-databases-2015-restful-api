package com.uantwerp.project_databases.benchmark.exception;

/**
 * @author aylin.gurkan,fatih.isler,firat.eren
 *
 *         Error reponse codes
 */
public enum ResponseCodes {
	SUCCESS(0, "Success"), ERROR(1, "Internal error"), DATA_NOT_FOUND(2, "Data not found");

	/**
	 * Default message for this error
	 */
	public final String message;
	/**
	 * 
	 */
	public final int errorCode;
	/**
	 * 
	 */
	public final boolean includeExceptionDetail;

	/**
	 * Constructor
	 */
	ResponseCodes() {
		message = "Sistem Hatası!";
		errorCode = 999;
		includeExceptionDetail = false;
	}

	/**
	 * Constructor
	 * 
	 * @param msg 		message of the response code
	 */
	ResponseCodes(String msg) {
		this.message = msg;
		this.errorCode = ordinal();
		this.includeExceptionDetail = false;
	}

	/**
	 * Constructor
	 * 
	 * @param errorCode 				Code of the error
	 * @param msg 						message of the exception
	 */
	ResponseCodes(int errorCode, String msg) {
		this.message = msg;
		this.errorCode = errorCode;
		this.includeExceptionDetail = false;
	}

	/**
	 * Constructor
	 * 
	 * @param errorCode 				error Code
	 * @param msg error 				message
	 * @param includeExceptionDetail 	Detail of the given exception
	 */
	ResponseCodes(int errorCode, String msg, boolean includeExceptionDetail) {
		this.message = msg;
		this.errorCode = errorCode;
		this.includeExceptionDetail = includeExceptionDetail;
	}

	/**
	 * 
	 * @return 		Message with the response code
	 */
	public String message() {
		return message;
	}

	/**
	 * Return the enumeration for the given ordinal
	 * 
	 * @param errorCode		Code of the error
	 * @return error
	 */
	public static ResponseCodes forErrorCode(int errorCode) {
		ResponseCodes[] errors = ResponseCodes.values();
		for (ResponseCodes ec : errors) {
			if (ec.errorCode == errorCode) {
				return ec;
			}
		}
		return ERROR;
	}

	/**
	 * Return the enumeration for the given ordinal
	 * 
	 * @param ordinal response code
	 * @return error
	 */
	public static ResponseCodes forOrdinal(int ordinal) {
		ResponseCodes[] errors = ResponseCodes.values();
		for (ResponseCodes ec : errors) {
			if (ec.ordinal() == ordinal) {
				return ec;
			}
		}
		return ERROR;
	}
}
