package com.uantwerp.project_databases.benchmark.db2.entity;

public class Adress {
	private String entityNumber, typeofaddress, countrynl, municipalitynl, streetnl, housenumber, extraaddressinfo;
	private String zipCode, box;

	public Adress() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Adress(String entityNumber, String typeofaddress, String countrynl, String municipalitynl, String streetnl,
			String housenumber, String extraaddressinfo, String zipCode, String box) {
		super();
		this.entityNumber = entityNumber;
		this.typeofaddress = typeofaddress;
		this.countrynl = countrynl;
		this.municipalitynl = municipalitynl;
		this.streetnl = streetnl;
		this.housenumber = housenumber;
		this.extraaddressinfo = extraaddressinfo;
		this.box = box;
		this.zipCode = zipCode;
	}

	public String getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(String entityNumber) {
		this.entityNumber = entityNumber;
	}

	public String getTypeofaddress() {
		return typeofaddress;
	}

	public void setTypeofaddress(String typeofaddress) {
		this.typeofaddress = typeofaddress;
	}

	public String getCountrynl() {
		return countrynl;
	}

	public void setCountrynl(String countrynl) {
		this.countrynl = countrynl;
	}

	public String getMunicipalitynl() {
		return municipalitynl;
	}

	public void setMunicipalitynl(String municipalitynl) {
		this.municipalitynl = municipalitynl;
	}

	public String getStreetnl() {
		return streetnl;
	}

	public void setStreetnl(String streetnl) {
		this.streetnl = streetnl;
	}

	public String getHousenumber() {
		return housenumber;
	}

	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}

	public String getExtraaddressinfo() {
		return extraaddressinfo;
	}

	public void setExtraaddressinfo(String extraaddressinfo) {
		this.extraaddressinfo = extraaddressinfo;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getBox() {
		return box;
	}

	public void setBox(String box) {
		this.box = box;
	}
	

}
