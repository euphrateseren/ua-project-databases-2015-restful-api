package com.uantwerp.project_databases.benchmark.db2.entity;

public class EnterpriseReal {
	private String enterpriseNumber, status, juridicalSituation, typeOfEnteerprise, juridicalForm, startDate;

	public EnterpriseReal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EnterpriseReal(String enterpriseNumber, String status, String juridicalSituation, String typeOfEnteerprise,
			String juridicalForm, String startDate) {
		super();
		this.enterpriseNumber = enterpriseNumber;
		this.status = status;
		this.juridicalSituation = juridicalSituation;
		this.typeOfEnteerprise = typeOfEnteerprise;
		this.juridicalForm = juridicalForm;
		this.startDate = startDate;
	}

	public String getEnterpriseNumber() {
		return enterpriseNumber;
	}

	public void setEnterpriseNumber(String enterpriseNumber) {
		this.enterpriseNumber = enterpriseNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getJuridicalSituation() {
		return juridicalSituation;
	}

	public void setJuridicalSituation(String juridicalSituation) {
		this.juridicalSituation = juridicalSituation;
	}

	public String getTypeOfEnteerprise() {
		return typeOfEnteerprise;
	}

	public void setTypeOfEnteerprise(String typeOfEnteerprise) {
		this.typeOfEnteerprise = typeOfEnteerprise;
	}

	public String getJuridicalForm() {
		return juridicalForm;
	}

	public void setJuridicalForm(String juridicalForm) {
		this.juridicalForm = juridicalForm;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


}
