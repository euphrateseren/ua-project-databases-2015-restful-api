package com.uantwerp.project_databases.benchmark.web;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.uantwerp.project_databases.benchmark.db2.entity.Activity;
import com.uantwerp.project_databases.benchmark.db2.entity.Adress;
import com.uantwerp.project_databases.benchmark.db2.entity.Contact;
import com.uantwerp.project_databases.benchmark.db2.entity.Denomination;
import com.uantwerp.project_databases.benchmark.db2.entity.EnterpriseAll;
import com.uantwerp.project_databases.benchmark.db2.entity.EnterpriseReal;
import com.uantwerp.project_databases.benchmark.db2.entity.Languages;
import com.uantwerp.project_databases.benchmark.db2.service.EnterpriseAllService;
import com.uantwerp.project_databases.benchmark.exception.ResponseCodes;
import com.uantwerp.project_databases.benchmark.exception.ServiceResponse;

@RestController
@RequestMapping(value = "kbo_data/api/v1")
/** Class that maps all the web services related to all the information of the enterprises excepts addresses and establishments
*
* @author gerito
* @version 1.0 2015
*/
public class EnterpriseAllController extends BaseController {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseAllController.class);

	@Autowired
	EnterpriseAllService enterpriseAllService;
	
	/**
	 * Get all the information of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1//establishments/{enterpriseName}/establishmentaddresses <br>
	 * @param enterpriseName	String with the name of the enterprise
	 * @return	Enterprise Object with all the available information of the enterprise
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/enterpriseall", method = RequestMethod.GET)
	EnterpriseAll getEnterpriseAllName(@PathVariable String enterpriseName) {
		return enterpriseAllService.getEnterpriseInfoName(enterpriseName);
	}
	
	/**
	 * Get all the information of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1//establishments/{enterpriseName}/establishmentaddresses <br>
	 * @param enterpriseNumber	String with the enterprise number of the enterprise
	 * @return	Enterprise Object with all the available information of the enterprise
	 */
	@RequestMapping(value = "/enterprises/{enterpriseNumber}/enterpriseall", method = RequestMethod.GET)
	EnterpriseAll getEnterpriseAllNumber(@PathVariable String enterpriseNumber) {
		EnterpriseAll enterp = enterpriseAllService.getEnterpriseInfo(enterpriseNumber);
		return enterp;
	}
	
	/**
	 * Get the enterprise Number based on the exact name of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1//establishments/{enterpriseName}/establishmentaddresses <br>
	 * @param enterpriseName	String with the name of the enterprise
	 * @return	Enterprise number/Vat number of the enterprise
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/vat", method = RequestMethod.GET)
	String getEnterpriseVat(@PathVariable String enterpriseName) {
		String vat = enterpriseAllService.getEnterpriseInfoName(enterpriseName).getEnterprise().getEnterpriseNumber();
		return vat;
	}

	/**
	 * Get the enterprise name based on the exact enterprise number of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/enterprises/{enterpriseNumber}/name <br>
	 * @param enterpriseNumber	String with the enterprise number of the enterprise
	 * @return	Name of the enterprise
	 */
	@RequestMapping(value = "/enterprises/{enterpriseNumber}/name", method = RequestMethod.GET)
	String getEnterpriseName(@PathVariable String enterpriseNumber) {
		List<Denomination> denomination =enterpriseAllService.getEnterpriseInfo(enterpriseNumber).getSocialInformation();
		String name = "";
		for (int i = 0; i < denomination.size(); i++) {
			Denomination element = denomination.get(i);
			if (element.getTypeOfDenomination().equals("Maatschappelijke naam")){				
				name = element.getDenomination();
			}
		}
		return name;	
	}
	
	/**
	 * Get the address of the enterprise based on the exact name of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/denominations/{enterpriseName}/enterprise/addresses <br>
	 * @param enterpriseName	String with the name of the enterprise
	 * @return	Address information of the enterprise
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/enterprise/addresses", method = RequestMethod.GET)
	Adress getEnterpriseAddressName(@PathVariable String enterpriseName) {
		return enterpriseAllService.getEnterpriseInfoName(enterpriseName).getAddress();
	}
	
	/**
	 * Get the address of the enterprise based on the exact number of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/enterprises/{enterpriseNumber}/enterprise/addresses <br>
	 * @param enterpriseNumber	String with the enterprise number of the enterprise
	 * @return	Address information of the enterprise
	 */
	@RequestMapping(value = "/enterprises/{enterpriseNumber}/enterprise/addresses", method = RequestMethod.GET)
	Adress getEnterpriseAddress(@PathVariable String enterpriseNumber) {
		return enterpriseAllService.getEnterpriseInfo(enterpriseNumber).getAddress();
	}
	
	/**
	 * Get the social information of the enterprise based on the exact name of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/denominations/{enterpriseName}/socialinformation <br>
	 * @param enterpriseName	String with the enterprise name of the enterprise
	 * @return	List of social information; related to the table Contacts
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/socialinformation", method = RequestMethod.GET)
	List<Contact> getEnterpriseSocialInfoName(@PathVariable String enterpriseName) {
		return enterpriseAllService.getEnterpriseInfoName(enterpriseName).getContacts();
	}
	
	/**
	 * Get the social information of the enterprise based on the exact number of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/enterprises/{enterpriseNumber}/socialinformation <br>
	 * @param enterpriseNumber	String with the enterprise number of the enterprise
	 * @return	List of social information; related to the table Contacts
	 */
	@RequestMapping(value = "/enterprises/{enterpriseNumber}/socialinformation", method = RequestMethod.GET)
	List<Contact> getEnterpriseSocialInfo(@PathVariable String enterpriseNumber) {
		return enterpriseAllService.getEnterpriseInfo(enterpriseNumber).getContacts();
	}
	
	/**
	 * Get the nace Number of the enterprise based on the exact name of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/denominations/{enterpriseName}/nace <br>
	 * @param enterpriseName	String with the enterprise name of the enterprise
	 * @return	Nace code of the enterprise; related to the table activity
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/nace", method = RequestMethod.GET)
	String getEnterpriseNaceName(@PathVariable String enterpriseName) {
		Activity activity = (Activity) enterpriseAllService.getEnterpriseInfoName(enterpriseName).getActivity().get(0);
		return activity.getNaceCode();
	}
	
	/**
	 * Get the nace number of the enterprise based on the exact number of the enterprise<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/enterprises/{enterpriseNumber}/nace <br>
	 * @param enterpriseNumber	String with the enterprise number of the enterprise
	 * @return	Nace code of the enterprise; related to the table activity
	 */
	@RequestMapping(value = "/enterprises/{enterpriseNumber}/nace", method = RequestMethod.GET)
	String getEnterpriseNace(@PathVariable String enterpriseNumber) {
		Activity activity =  (Activity) enterpriseAllService.getEnterpriseInfo(enterpriseNumber).getActivity().get(0);
		return activity.getNaceCode();
	}
	
	/**
	 * Web service in charge of giving the status of an enterprise based in an enterprise name<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/denominations/{enterpriseName}/status <br>
	 * @param enterpriseName	String with the enterprise name of the enterprise
	 * @return	Status of the enterprise
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/status", method = RequestMethod.GET)
	String getEnterpriseStatusName(@PathVariable String enterpriseName) {
		return enterpriseAllService.getEnterpriseInfoName(enterpriseName).getEnterprise().getStatus();
	}
	
	/**
	 * Web service in charge of giving the status of an enterprise based in an enterprise number<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/enterprises/{enterpriseNumber}/status <br>
	 * @param enterpriseNumber	String with the enterprise number of the enterprise
	 * @return	Status of the enterprise
	 */
	@RequestMapping(value = "/enterprises/{enterpriseNumber}/status", method = RequestMethod.GET)
	String getEnterpriseStatus(@PathVariable String enterpriseNumber) {
		return  enterpriseAllService.getEnterpriseInfo(enterpriseNumber).getEnterprise().getStatus();
	}
	
	/**
	 * Web service in charge of giving a list of the enterprise names alike of an entered parameter<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/denominations/{enterpriseName}/like <br>
	 * @param enterpriseName	String with the enterprise name of the enterprise the user is looking for
	 * @return	List of String with all the enterprise with name alike the parameter entered
	 */
	@RequestMapping(value = "/denominations/{enterpriseName}/like", method = RequestMethod.GET)
	List<String> getEnterpriseNameLike(@PathVariable String enterpriseName) {
		List<Denomination> denomination =  enterpriseAllService.getDenominationLike(enterpriseName);
		List<String> nombres = new ArrayList<String>();
		for (int i = 0; i < denomination.size(); i++) {
			Denomination element = denomination.get(i);
			nombres.add(element.getDenomination());
		}
	return nombres;
	}
	
	/**
	 * Web service in charge of giving the description of a Code for the descriptors items of the database<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/languages/{codeID}/code <br>
	 * @param codeID	String with the code Id of the table codes
	 * @return	Description of the entered code Id
	 */
	@RequestMapping(value = "/languages/{codeID}/code", method = RequestMethod.GET)
	List<Languages> getLanguageCodeId(@PathVariable String codeID) {
		return  enterpriseAllService.getLanguageCodeId(codeID);
	}
	
	/**
	 * Web service in charge of giving the Code ID of a description for the descriptors items of the database<br>
	 * Method GET <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/languages/{description}/description <br>
	 * @param description	String with the description of a Code of the table Code
	 * @return	The code Id of the entered description
	 */
	@RequestMapping(value = "/languages/{description}/description", method = RequestMethod.GET)
	List<Languages> getLanguageDescription(@PathVariable String description) {
		return  enterpriseAllService.getLanguageDescription(description);
	}
	
	/**
	 * Web service in charge of adding a Contact in the table Contact
	 * @param contact	Object of type Contact; related to the social information of the enterprise<br>
	 * Method POST <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/contacts <br>
	 * @return	boolean with the response code for the insertion
	 */
	@RequestMapping(value = "/contacts", method = RequestMethod.POST, consumes = "application/json")
	public ServiceResponse addContact(@RequestBody Contact contact) {
		boolean isUpdated = enterpriseAllService.addContact(createContactSup(contact));
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	/**
	 * Web service in charge of removing a Contact in the table Contact<br>
	 * Method DELETE <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/contacts <br>
	 * @param contact	Object of type Contact; related to the social information of the enterprise
	 * @return	boolean with the response code for the deletion
	 */
	@RequestMapping(value = "/contacts", method = RequestMethod.DELETE, consumes = "application/json")
	public ServiceResponse removeContact(@RequestBody Contact contact) {
		boolean isUpdated = enterpriseAllService.removeContact(createContactSup(contact));
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	/**
	 * Web service in charge of adding a Contact in the table Contact<br>
	 * Method POST <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/contacts/{entityNumber}/{entityContact}/{contactType}/{value} <br>
	 * @param entityNumber		number of the enterprise 
	 * @param entityContact		entity contact; value of table Code/Languages
	 * @param contactType		contact type; value of table Code/Languages
	 * @param value				value for the contact
	 * @return	boolean with the response code for the insertion
	 */
	@RequestMapping(value = "/contacts/{entityNumber}/{entityContact}/{contactType}/{value}", method = RequestMethod.POST)
	public ServiceResponse addContact(@PathVariable String entityNumber,@PathVariable String entityContact,@PathVariable String contactType,@PathVariable String value) {
		Contact contact = new Contact(entityNumber, entityContact, contactType, value);
		boolean isUpdated = enterpriseAllService.addContact(createContactSup(contact));
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	public Contact createContactSup(Contact contact){
		String entityContact;
		if (contact.getEntityContact()==null)
			entityContact = null;
		else{
			List<Languages> languages =  enterpriseAllService.getLanguageDescription(contact.getEntityContact());
			if (!languages.isEmpty())
				entityContact = languages.get(0).getCodeid();
			else
				entityContact = null;
		}
		String contactType;
		if (contact.getContactType()==null)
			contactType = null;
		else{
			List<Languages> languages =  enterpriseAllService.getLanguageDescription(contact.getContactType());
			if (!languages.isEmpty())
				contactType = languages.get(0).getCodeid();
			else
				contactType = null;
		}
		Contact contact1 = new Contact(contact.getEntityNumber(), 
									   entityContact, 
									   contactType, 
									   contact.getValue());
		return contact1;
	}
	
	/**
	 * Web service in charge of removing a Contact in the table Contact<br>
	 * Method DELETE <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/contacts/{entityNumber}/{entityContact}/{contactType} <br>
	 * @param entityNumber		number of the enterprise 
	 * @param entityContact		entity contact; value of table Code/Languages
	 * @param contactType		contact type; value of table Code/Languages
	 * @return	boolean with the response code for the deletion
	 */
	@RequestMapping(value = "/contacts/{entityNumber}/{entityContact}/{contactType}", method = RequestMethod.DELETE)
	public ServiceResponse removeContact(@PathVariable String entityNumber,@PathVariable String entityContact,@PathVariable String contactType) {
		Contact contact = new Contact(entityNumber, entityContact, contactType, null);
		Contact contact1 = createContactSup(contact);
		boolean isUpdated = enterpriseAllService.removeContact(entityNumber, 
													contact1.getContactType(), 
													contact1.getEntityContact());
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	/**
	 * Web service in charge of adding an Activity in the table Activity<br>
	 * Method POST <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/activities <br>
	 * @param activity	Object of type Activity; related to the activity and nace code on the table Activity
	 * @return	boolean with the response code for the insertion
	 */
	@RequestMapping(value = "/activities", method = RequestMethod.POST, consumes = "application/json")
	public ServiceResponse addActivity(@RequestBody Activity activity) {
		Activity activity1 = createActivitySup(activity);
		boolean isUpdated = enterpriseAllService.addActivity(activity1);
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	/**
	 * Web service in charge of removing an Activity in the table Activity<br>
	 * Method DELETE <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/activities <br>
	 * @param activity	Object of type Activity; related to the activity and nace code on the table Activity
	 * @return	boolean with the response code for the deletion
	 */
	@RequestMapping(value = "/activities", method = RequestMethod.DELETE, consumes = "application/json")
	public ServiceResponse removeActivity(@RequestBody Activity activity) {
		Activity activity1 = createActivitySup(activity);
		boolean isUpdated = enterpriseAllService.removeActivity(activity1);
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	public Activity createActivitySup(Activity activity){
		String activityGroup;
		List<Languages> languages =  enterpriseAllService.getLanguageDescription(activity.getActivityGroup());
		if (!languages.isEmpty())
			activityGroup = languages.get(0).getCodeid();
		else
			activityGroup = null;
		String classification;
		if (activity.getClassification()==null)
			classification = null;
		else{
			List<Languages> languages2 = enterpriseAllService.getLanguageDescription(activity.getClassification());
			if (!languages2.isEmpty())
				classification = languages2.get(0).getCodeid();
			else
				classification = null;
		}
		String naceVersion;
		if (activity.getNaceVersion()==null)
			naceVersion = null;
		else{
			List<Languages> languages3 = enterpriseAllService.getLanguageDescription(activity.getNaceVersion());
			if (!languages3.isEmpty())
				naceVersion =languages3.get(0).getCodeid();
			else
				naceVersion = null;
		}
		Activity activity1 = new Activity(activity.getEntityNumber(), 
										  activityGroup, 
										  classification, 
										  naceVersion, 
										  activity.getNaceCode());
		return activity1;
	}
	/**
	 * Web service in charge of adding an Activity in the table Activity<br>
	 * Method POST <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/activities/{entityNumber}/{activityGroup}/{classification}/{naceVersion}/{naceCode} <br>
	 * @param entityNumber		enterprise number/vat number
	 * @param activityGroup		activity group from table code/languages
	 * @param naceVersion		nace version from table code/languages
	 * @param naceCode 			nace Code
	 * @param classification	classification group from table code/languages
	 * @return	boolean with the response code for the insertion
	 */
	@RequestMapping(value = "/activities/{entityNumber}/{activityGroup}/{classification}/{naceVersion}/{naceCode}", method = RequestMethod.POST)
	public ServiceResponse addActivity(@PathVariable String entityNumber, @PathVariable String activityGroup, @PathVariable String classification, @PathVariable String naceVersion, String naceCode) {
		Activity activity = new Activity(entityNumber, activityGroup, classification, naceVersion, naceCode);
		Activity activity1 = createActivitySup(activity);
		boolean isUpdated = enterpriseAllService.addActivity(activity1.getEntityNumber(), 
										activity1.getActivityGroup(), 
										activity1.getNaceVersion(), 
										activity1.getNaceCode(), 
										activity1.getClassification());
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	/**
	 * Web service in charge of removing an Activity in the table Activity<br>
	 * Method DELETE <br>
	 * Address: http://houweel.uantwerpen.be:8090/kbo_data/api/v1/activities/{entityNumber}/{activityGroup}/{naceCode} <br>
	 * @param entityNumber		enterprise number/vat number
	 * @param activityGroup		activity group from table code/languages
	 * @param naceCode 			nace Code
	 * @return	boolean with the response code for the deletion
	 */
	@RequestMapping(value = "/activities/{entityNumber}/{activityGroup}/{naceCode}", method = RequestMethod.DELETE)
	public ServiceResponse removeActivity(@PathVariable String entityNumber,@PathVariable String activityGroup,@PathVariable String naceCode) {
		System.out.println(activityGroup);
		String activityGroup2 = enterpriseAllService.getLanguageDescription(activityGroup).get(0).getCodeid();
		boolean isUpdated = enterpriseAllService.removeActivity(entityNumber, 
												activityGroup2, 
												naceCode);
		if (isUpdated)
			return new ServiceResponse(ResponseCodes.SUCCESS);
		else
			return new ServiceResponse(ResponseCodes.DATA_NOT_FOUND);
	}
	
	

}