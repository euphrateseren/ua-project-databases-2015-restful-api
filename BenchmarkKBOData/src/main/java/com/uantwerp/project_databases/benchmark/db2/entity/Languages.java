package com.uantwerp.project_databases.benchmark.db2.entity;

public class Languages {
	private String codeid, language, description;

	public Languages(String codeid, String language, String description) {
		super();
		this.codeid = codeid;
		this.language = language;
		this.description = description;
	}

	public Languages() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCodeid() {
		return codeid;
	}

	public void setCodeid(String codeid) {
		this.codeid = codeid;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
