package com.uantwerp.project_databases.benchmark.db2;

public interface BenchmarkData {
	
	public static final int COUNT = 10; 

	public void selectFromWhere();

	public void selectAll();

	public void insertOne();

	public void insertMany();
	
	public void updateOne();

	public void updateMany();

	public void deleteOne();

	public void deleteMany();
	
	public void selectWithJoin();
}
