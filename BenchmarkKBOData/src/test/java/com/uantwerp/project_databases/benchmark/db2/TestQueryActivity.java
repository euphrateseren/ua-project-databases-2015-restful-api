package com.uantwerp.project_databases.benchmark.db2;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;

import com.uantwerp.project_databases.benchmark.Application;
//import com.uantwerp.project_databases.benchmark.Application;
import com.uantwerp.project_databases.benchmark.db2.entity.QueryActivity;
import com.uantwerp.project_databases.benchmark.db2.repo.QueryActivityRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class TestQueryActivity{// implements BenchmarkData{
	
	public static final int COUNT = 1; 
	
	@Autowired
	private QueryActivityRepository queryRepository;

	
	@Test
	public void mainTest(){
		for (int i = 0; i <  COUNT;i++){
			selectWithJoin();
		}
	}
	
	
	public void selectWithJoin() {
		long startTime = System.currentTimeMillis();
		List<QueryActivity> a = queryRepository.SelectQuery();
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime-startTime;
		System.out.println("time selectAll " + String.valueOf(realTime));
	}
	
}
