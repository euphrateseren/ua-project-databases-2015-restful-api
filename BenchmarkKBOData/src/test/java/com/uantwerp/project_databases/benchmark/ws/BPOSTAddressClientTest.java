package com.uantwerp.project_databases.benchmark.ws;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.uantwerp.project_databases.benchmark.Application;
import com.uantwerp.project_databases.benchmark.db2.entity.Adress;

import bpost_production.wsdl.ValidatedAddressResultType;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class BPOSTAddressClientTest {

	@Test
	public void validateAddress() {
		Adress address = new Adress();
		address.setEntityNumber("2.238.153.749");
		address.setMunicipalitynl("Schaarbeek");
		address.setHousenumber("161");
		address.setStreetnl("Emile Maxlaan");
		// address.setBox("");
		// address.setZipCode("1030");

		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("bpost_production.wsdl");
		BPOSTAddressClient client = new BPOSTAddressClient();
		client.setDefaultUri("https://webservices-pub.bpost.be/ws/ExternalMailingAddressProofingCS_v1");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);

		ValidatedAddressResultType result = client.validateAddress(address);
		Assert.assertNotNull(result);

	}
}
