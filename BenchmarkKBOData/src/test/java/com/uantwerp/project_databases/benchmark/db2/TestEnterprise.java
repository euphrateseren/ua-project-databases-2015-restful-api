package com.uantwerp.project_databases.benchmark.db2;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;

import com.uantwerp.project_databases.benchmark.Application;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;
import com.uantwerp.project_databases.benchmark.db2.repo.EnterpriseRepository;
//import com.uantwerp.project_databases.benchmark.Application;
import com.uantwerp.project_databases.benchmark.db2.repo.QueryActivityRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class TestEnterprise{// implements BenchmarkData{
	
	public static final int COUNT = 1; 
	
	@Autowired
	private EnterpriseRepository enterpriseRepository;

	
	@Test
	public void mainTest(){
		for (int i = 0; i <  COUNT;i++){
			selectEnterpriseWithNumber();
			selectEnterpriseWithNextNumber();
		}
	}
	
	
	public void selectEnterpriseWithNumber() {
			long startTime = System.currentTimeMillis();
			List<Enterprise> a = enterpriseRepository.selectQueryWithEnterpriseNumber(200239951);
			long stopTime = System.currentTimeMillis();
			long realTime = stopTime-startTime;
			System.out.println("time selectAll " + String.valueOf(realTime));
	}
	
	public void selectEnterpriseWithNextNumber() {
		long startTime = System.currentTimeMillis();
		List<Enterprise> a = enterpriseRepository.selectQueryNextEnterpriseNumber(200239951);
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime-startTime;
		System.out.println("time selectAll next" + String.valueOf(realTime));
}

	
	
}
