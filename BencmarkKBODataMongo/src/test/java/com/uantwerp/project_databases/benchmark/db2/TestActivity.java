package com.uantwerp.project_databases.benchmark.db2;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;

import com.uantwerp.project_databases.benchmark.Application;
//import com.uantwerp.project_databases.benchmark.Application;
import com.uantwerp.project_databases.benchmark.db2.entity.Activity;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;
import com.uantwerp.project_databases.benchmark.db2.entity.TestDBInsert;
import com.uantwerp.project_databases.benchmark.db2.repo.ActivityRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class TestActivity implements BenchmarkData {// implements BenchmarkData{

	public static final int COUNT = 10;

	@Autowired
	private ActivityRepository activityRepository;

	@Test
	public void mainTest() {
		for (int i = 0; i < COUNT; i++) {
			selectFromWhere(i);
//			selectAll();
			insertMany();
			insertOne(i);
			updateOne(i);
			updateMany();
			deleteOne(i);
			deleteMany();
		}
	}

	public void selectFromWhere(int i) {
		String activityGroup = "PP0001";
		long startTime = System.currentTimeMillis();
		List<Activity> a = activityRepository.findByActivityGroup(activityGroup);
		List<TestDBInsert> b = activityRepository.findByActivityGroup2();
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime - startTime;
		// activityRepository.InsertResults("selectAll", i, realTime, "QUERY");
		System.out.println("time selectFromWhere " + realTime);
	}

	public void selectAll() {
		long startTime = System.currentTimeMillis();
		List<Activity> a = activityRepository.SelectAllData();
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime - startTime;
		// activityRepository.InsertResults("selectAll", i, realTime, "QUERY");
		System.out.println("time selectAll " + realTime);
	}

	public void insertOne(int i) {
		long startTime = System.currentTimeMillis();
		activityRepository.InsertOne(1001 + i);
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime - startTime;
		// activityRepository.InsertResults("insertOne", i, realTime, "INSERT");
		System.out.println("time insertOne " + realTime);
	}

	//
	//
	public void insertMany() {
		long startTime = System.currentTimeMillis();
		activityRepository.InsertAllData(1000);
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime - startTime;
		// activityRepository.InsertResults("insertMany", i, realTime,
		// "INSERT");
		System.out.println("time insertMany " + realTime);
	}

	//
	//
	public void updateOne(int i) {
		long startTime = System.currentTimeMillis();
		activityRepository.UpdateOne(1001 + i);
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime - startTime;
		// activityRepository.InsertResults("updateOne", i, realTime, "UPDATE");
		System.out.println("time updateOne " + realTime);
	}

	//
	//
	public void updateMany() {
		long startTime = System.currentTimeMillis();
		activityRepository.UpdateAllData();
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime - startTime;
		// activityRepository.InsertResults("updateMany", i, realTime,
		// "UPDATE");
		System.out.println("time updateMany " + realTime);
	}

	//
	//
	public void deleteOne(int i) {
		long startTime = System.currentTimeMillis();
		activityRepository.DeleteOne(i);
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime - startTime;
		// activityRepository.InsertResults("deleteOne", i, realTime, "DELETE");
		System.out.println("time deleteOne " + realTime);
	}

	//
	//
	public void deleteMany() {
		long startTime = System.currentTimeMillis();
		activityRepository.DeleteAllData();
		long stopTime = System.currentTimeMillis();
		long realTime = stopTime - startTime;
		// activityRepository.InsertResults("deleteMany", i, realTime,
		// "DELETE");
		System.out.println("time deleteMany " + realTime);
	}

	@Override
	public void selectWithJoin() {
		// TODO Auto-generated method stub

	}

	/*
	 * public void selectSpecial(int i){ long startTime =
	 * System.currentTimeMillis(); try { activityRepository.selectSpecial(); }
	 * catch (SQLException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } long stopTime = System.currentTimeMillis(); long
	 * realTime = stopTime-startTime;
	 * activityRepository.InsertResults("selectSpecial", i, realTime, "SELECT");
	 * System.out.println("time selectSpecial " + realTime); }
	 */
}
