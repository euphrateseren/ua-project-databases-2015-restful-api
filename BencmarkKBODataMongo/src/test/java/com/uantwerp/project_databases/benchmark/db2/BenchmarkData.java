package com.uantwerp.project_databases.benchmark.db2;

public interface BenchmarkData {
	
	public static final int COUNT = 10; 

	public void selectFromWhere(int i);

	public void selectAll();

	public void insertOne(int i);

	public void insertMany();
	
	public void updateOne(int i);

	public void updateMany();

	public void deleteOne(int i);

	public void deleteMany();
	
	public void selectWithJoin();
}
