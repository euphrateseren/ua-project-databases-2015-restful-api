package com.uantwerp.project_databases.benchmark.db2.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

//import javax.management.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

//import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
import com.uantwerp.project_databases.benchmark.db2.entity.Activity;
import com.uantwerp.project_databases.benchmark.db2.entity.Enterprise;
import com.uantwerp.project_databases.benchmark.db2.entity.TestDBInsert;

@Repository("activityRepository")
public class ActivityRepository {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private MongoOperations mongoOperations;

	// private static final RowMapper<Activity> activityRowMapper = new
	// RowMapper<Activity>() {
	// @Override
	// public Activity mapRow(ResultSet rs, int rowNum) throws SQLException {
	// Activity activity = new Activity(rs.getString("ENTITYNUMBER"),
	// rs.getString("ACTIVITYGROUP"),
	// rs.getString("NACEVERSION"), rs.getString("NACECODE"),
	// rs.getString("CLASSIFICATION"));
	// return activity;
	// }
	// };

	public List<Activity> findByActivityGroup(String activityGroup) {
		// return mongoTemplate.findAll(Activity.class);
		return mongoTemplate.find(new Query(Criteria.where("ActivityGroup").is(activityGroup)), Activity.class);
	}

	public List<TestDBInsert> findByActivityGroup2() {
		// return mongoTemplate.findAll(Activity.class);
		return mongoTemplate.find(new Query(Criteria.where("Cnumber").is("1006")), TestDBInsert.class);
	}

	public void InsertAllData(long numData) {
		for (int i = 0; i < numData; i++) {

			TestDBInsert object = new TestDBInsert();
			object.setCnumber("" + i);
			object.setCtext("Text " + i);
			mongoTemplate.insert(object);
		}
	}

	public List<Activity> SelectAllData() {
		return mongoTemplate.findAll(Activity.class);
	}

	public void InsertOne(long dataToInsert) {
		TestDBInsert object = new TestDBInsert();
		object.setCnumber("" + dataToInsert);
		object.setCtext("Text " + dataToInsert);
		mongoTemplate.insert(object);
	}

	public void UpdateOne(long dataToUpdate) {

		mongoTemplate.updateFirst(new Query(Criteria.where("cnumber").is("" + dataToUpdate)),
				Update.update("ctext", dataToUpdate + "UPDATED"), TestDBInsert.class);
	}

	//
	public void UpdateAllData() {
		mongoTemplate.updateMulti(new Query(Criteria.where("cnumber").nin()), Update.update("ctext", "UPDATED ALL"),
				TestDBInsert.class);
	}

	public void DeleteOne(long dataToDelete) {
		// this.jdbcTemplate.update("delete from SCHDBENTERP.TESTDBINSERT where
		// CNUMBER = ?", dataToDelete);

		mongoTemplate.remove(new Query(Criteria.where("cnumber").is("" + dataToDelete)), TestDBInsert.class);
	}

	public void DeleteAllData() {
		mongoTemplate.remove(new Query(Criteria.where("cnumber").nin()), TestDBInsert.class);
	}
	//
	// public List<Activity> SelectfROMjOIN(){
	// return jdbcTemplate.query("SELECT ENTITYNUMBER, FROM
	// SCHDBENTERP.ACTIVITY",
	// activityRowMapper);
	// }

	/*
	 * public void selectSpecial() throws SQLException { System.out.println(
	 * "ingresa 1"); try { Class.forName("com.ibm.db2.jcc.DB2Driver"); } catch
	 * (ClassNotFoundException e) { e.printStackTrace(); } String url =
	 * "jdbc:db2://houweel.uantwerpen.be:50100/DBENTERP"; Connection con =
	 * DriverManager.getConnection(url, "pdb1", "PDB1050205"); ResultSet
	 * executeQuery = con.prepareStatement(
	 * "SELECT A.ENTERPRISENUMBER , C.DESCRIPTION ACTIVITY, D.DESCRIPTION CLASSIFICATION FROM SCHDBENTERP.ENTERPRISE A, SCHDBENTERP.ACTIVITY B, SCHDBENTERP.LANGUAGES C, SCHDBENTERP.LANGUAGES D WHERE A.ENTERPRISENUMBER = B.ENTITYNUMBER AND  B.CLASSIFICATION = C.CODE_ID AND B.ACTIVITYGROUP = D.CODE_ID AND C.LANGUAGE = 'NL' AND D.LANGUAGE = 'NL' AND A.ENTERPRISENUMBER = '0413.814.569'"
	 * ).executeQuery(); //while(executeQuery.next()) {
	 * //System.out.print(executeQuery.getString("ENTERPRISENUMBER")+ " ");
	 * //System.out.print(executeQuery.getString("ACTIVITY")+ " ");
	 * //System.out.print(executeQuery.getString("CLASSIFICATION")+ " ");
	 * //System.out.println(); //} }
	 */

}
