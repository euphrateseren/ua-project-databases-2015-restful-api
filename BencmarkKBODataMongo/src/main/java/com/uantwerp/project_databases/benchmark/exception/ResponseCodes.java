package com.uantwerp.project_databases.benchmark.exception;

/**
 * @author aylin.gurkan,fatih.isler,firat.eren
 *
 * Error reponse codes
 */
public enum ResponseCodes {
	SUCCESS(0, "İşleminiz başarıyla gerçekleştirildi"),
	ERROR(1, "Sistemsel bir hata oluştu!"),
	SURVEYS_NOT_FOUND(2, "Aktif anket bulunmamaktadır"),
	SURVEY_USER_NOT_FOUND(2, "Anket detayına erişilememektedir"),
	USER_NAME_PASSWORD_NOT_VALID(3, "Kullanıcı Adı/Şifresi dogrulanamadı!"),
	USER_ACTIVE_MAIL_NOT_FOUND(4, "İlgili kullanıcının aktif e-mail adresi bulunamadığından bu işlemi geçekleştiremezsiniz!"),
	REQUEST_MESSAGE_INVALID(5, "Hatalı İstek!"),
	ANSWER_SAVE_ERROR(6, "Cevaplarınız kaydedilirken bir hata oluştu"),
	QUESTION_NOT_FOUND(7,"Bu id ile girilmiş bir soru bulunmamaktadır"),
	TEMPLATE_SAVE_ERROR(8,"Anket taslağı kaydedilememiştir"),
	QUESTION_SAVE_ERROR(9,"Sorular kaydedilememiştir"),
	ANSWER_TYPE_ERROR(10,"Böyle bir cevap tipi bulunmamaktadır."),
	TEMPLATES_NOT_FOUND(11,"Hiç bir anket taslağı oluşturulmamıştır."),
	SURVEY_FILTER_ERROR(12,"Anket filtresi kaydedilememiştir."),
	SURVEY_SAVE_ERROR(13,"Anket kaydedilememiştir.");
	

	
	/**
	 * Default message for this error
	 */
	public final String message;
	/**
	 * 
	 */
	public final int errorCode;
	/**
	 * 
	 */
	public final boolean includeExceptionDetail;

	/**
	 * Constructor
	 */
	ResponseCodes() {
		message="Sistem Hatası!";
		errorCode=999;
		includeExceptionDetail=false;
	}

	/**
	 * Constructor
	 * 
	 * @param msg
	 */
	ResponseCodes(String msg) {
		this.message = msg;
		this.errorCode = ordinal();
		this.includeExceptionDetail=false;
	}

	/**
	 * Constructor
	 * 
	 * @param errorCode
	 * @param msg
	 * @param includeExceptionDetail
	 */
	ResponseCodes(int errorCode, String msg) {
		this.message = msg;
		this.errorCode = errorCode;
		this.includeExceptionDetail=false;
	}

	/**
	 * Constructor
	 * @param errorCode
	 * @param msg
	 * @param includeExceptionDetail
	 */
	ResponseCodes(int errorCode, String msg, boolean includeExceptionDetail) {
		this.message = msg;
		this.errorCode = errorCode;
		this.includeExceptionDetail = includeExceptionDetail;
	}

	/**
	 * 
	 * @return
	 */
	public String message() {
		return message;
	}

	/**
	 * Return the enumeration for the given ordinal
	 * 
	 * @param ordinal
	 * @return
	 */
	public static ResponseCodes forErrorCode(int errorCode) {
		ResponseCodes[] errors = ResponseCodes.values();
		for (ResponseCodes ec : errors) {
			if (ec.errorCode == errorCode) {
				return ec;
			}
		}
		return ERROR;
	}

	/**
	 * Return the enumeration for the given ordinal
	 * 
	 * @param ordinal
	 * @return
	 */
	public static ResponseCodes forOrdinal(int ordinal) {
		ResponseCodes[] errors = ResponseCodes.values();
		for (ResponseCodes ec : errors) {
			if (ec.ordinal() == ordinal) {
				return ec;
			}
		}
		return ERROR;
	}
}
