package com.uantwerp.project_databases.benchmark.db2.entity;

public class Languages {
	private String codeId, Language, Description;

	public Languages() {
	}

	public Languages(String codeId, String language, String description) {
		super();
		this.codeId = codeId;
		Language = language;
		Description = description;
	}
	
	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

}

