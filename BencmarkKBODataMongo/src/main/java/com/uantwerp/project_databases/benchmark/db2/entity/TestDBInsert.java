package com.uantwerp.project_databases.benchmark.db2.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "testdbinsert")
public class TestDBInsert {
	private String cnumber, ctext;

	public String getCnumber() {
		return cnumber;
	}

	public void setCnumber(String cnumber) {
		this.cnumber = cnumber;
	}

	public String getCtext() {
		return ctext;
	}

	public void setCtext(String ctext) {
		this.ctext = ctext;
	}
}
