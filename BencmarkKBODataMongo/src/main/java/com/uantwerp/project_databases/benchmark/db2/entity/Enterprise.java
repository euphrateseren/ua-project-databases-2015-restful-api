package com.uantwerp.project_databases.benchmark.db2.entity;

public class Enterprise {
	private String denomination, contact_web, contact_mail;
	private long enterprisenumber;
	
	public Enterprise(long enterprisenumber,String denomination, String contact_web, String contact_mail) {
		super();
		this.denomination = denomination;
		this.contact_web = contact_web;
		this.contact_mail = contact_mail;
		this.enterprisenumber = enterprisenumber;
	}

	public Enterprise() {
	}

	
	public long getEnterprisenumber() {
		return enterprisenumber;
	}

	public void setEnterprisenumber(long enterprisenumber) {
		this.enterprisenumber = enterprisenumber;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getContact_web() {
		return contact_web;
	}

	public void setContact_web(String contact_web) {
		this.contact_web = contact_web;
	}

	public String getContact_mail() {
		return contact_mail;
	}

	public void setContact_mail(String contact_mail) {
		this.contact_mail = contact_mail;
	};
	
	
	
}
