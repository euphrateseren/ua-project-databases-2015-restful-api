package com.uantwerp.project_databases.benchmark.db2.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "activity")
public class Activity {
	private String entityNumber, activityGroup, classification;
	private String naceVersion, naceCode;

	public Activity() {
	}

	public Activity(String entityNumber, String activityGroup, String classification, String naceVersion,
			String naceCode) {
		super();
		this.entityNumber = entityNumber;
		this.activityGroup = activityGroup;
		this.classification = classification;
		this.naceVersion = naceVersion;
		this.naceCode = naceCode;
	}

	public String getEntityNumber() {
		return entityNumber;
	}

	public void setEntityNumber(String entityNumber) {
		this.entityNumber = entityNumber;
	}

	public String getActivityGroup() {
		return activityGroup;
	}

	public void setActivityGroup(String activityGroup) {
		this.activityGroup = activityGroup;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getNaceVersion() {
		return naceVersion;
	}

	public void setNaceVersion(String naceVersion) {
		this.naceVersion = naceVersion;
	}

	public String getNaceCode() {
		return naceCode;
	}

	public void setNaceCode(String naceCode) {
		this.naceCode = naceCode;
	}

}
