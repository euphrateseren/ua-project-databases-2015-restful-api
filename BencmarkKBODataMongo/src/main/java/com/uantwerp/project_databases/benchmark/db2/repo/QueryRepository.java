//package com.uantwerp.project_databases.benchmark.db2.repo;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Repository;
//
////import com.uantwerp.project_databases.benchmark.db2.BenchmarkData;
//import com.uantwerp.project_databases.benchmark.db2.entity.Query1;
//
//@Repository("queryRepository")
//public class QueryRepository {
//
//	@Autowired
//	private JdbcTemplate jdbcTemplate;
//
//	private static final RowMapper<Query1> queryRowMapper = new RowMapper<Query1>() {
//		@Override
//		public Query1 mapRow(ResultSet rs, int rowNum) throws SQLException {
//			Query1 activity = new Query1(rs.getString("ENTITYNUMBER"), rs.getString("ACTIVITY"),
//					rs.getString("CLASSIFICATION"));
//			return activity;
//		}
//	};
//
//	public List<Query1> SelectQuery() {
//		return jdbcTemplate.query("SELECT A.ENTERPRISENUMBER ENTITYNUMBER, C.DESCRIPTION ACTIVITY, D.DESCRIPTION CLASSIFICATION "
//								+ "FROM SCHDBENTERP.ENTERPRISE A, SCHDBENTERP.ACTIVITY B, SCHDBENTERP.LANGUAGES C, SCHDBENTERP.LANGUAGES D "
//								+ "WHERE A.ENTERPRISENUMBER = B.ENTITYNUMBER "
//								+ "AND  B.CLASSIFICATION = C.CODE_ID "
//								+ "AND B.ACTIVITYGROUP = D.CODE_ID "
//								+ "AND C.LANGUAGE = 'NL' AND D.LANGUAGE = 'NL' "
//								+ "AND A.ENTERPRISENUMBER = '0413.814.569'",
//				queryRowMapper);
//	}
//}
